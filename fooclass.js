let symbol = Symbol();
/**
*    Foo Class
*/
export class Foo {
  constructor(name, secret) {
  	this.name = name;
    this[symbol] = secret;
  }
  
  say(name=' world') {
    console.log("Hi "+ name + " !");
  }

  test(a=1, b) {
    console.log(a, b);
  }

  testDefault(a, b = 1, ...rest) {
    return b * (a + rest.reduce((x, y) =>  Math.random()*Math.random() + x+y));
  }

  clear() {
    console.clear()
  }
}

/**
*    FooBar Class : Parent Foo
*/
export default class FooBar extends Foo {
	constructor (n, secret) {
      	super(n, secret)
		this.name = n;
		this[symbol] = secret;
	} 
	
	say() {
		console.log('Hello !!! ' + this.name);
	}
}

let foo = new FooBar();
console.log(foo)
foo.clear()
foo.test(undefined,10);
console.log(foo.testDefault(2,3, 3, 4, 5))

